# Danger CI/CD

## Danger CI

This project allow to check your commit syntax project by including
it into your `.gitlab-ci.yml`.

Example:
```
include:
  - project: 'aerian/cicd/danger'
    ref: main
    file: 'danger-ci.yml'

stages:
  - checker
```

### Requirements

You will need either the file `.releaserc.json` to be present in your project
or as a variable `RELEASERC_FILE`. Please customize the file based on your needs.

## Automatical version publishing

Please note that this project use semantic-release for auto incrementation of
version.
If you want to build a version, you should use the Angular commit syntax.

More informations at:
https://github.com/semantic-release/semantic-release#commit-message-format
